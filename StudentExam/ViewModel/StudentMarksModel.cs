﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StudentExam.ViewModel
{
    public class StudentMarksModel
    {
        public int StudentId { get; set; }
        public string SubjectName { get; set; }
        public int? TotalMarks { get; set; }
        public int? MarksObtained { get; set; }
        public double? Percentage { get; set; }




    }
}