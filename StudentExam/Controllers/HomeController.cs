﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using StudentExam.Models;
using StudentExam.ViewModel;
namespace StudentExam.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        StudentDBEntities objStudentDBEntities;
        public HomeController()
        {
            objStudentDBEntities= new StudentDBEntities();
        }
        public ActionResult Index()
        {
            StudentMasterViewModel ObjMasterViewModel = new StudentMasterViewModel();
            ObjMasterViewModel.ListOfExams = (from obj in objStudentDBEntities.Exams
                                              select new SelectListItem()
                                              {
                                                  Text = obj.ExamName,
                                                  Value = obj.ExamID.ToString()
                                              }).ToList();

            ObjMasterViewModel.ListOfSubject = (from obj in objStudentDBEntities.Subjects
                                                select new SelectListItem()
                                                {
                                                    Text = obj.SubjectName,
                                                    Value = obj.SubjectId.ToString()
                                                }).ToList();




            ObjMasterViewModel.ListOfStudentModel = (from objstu in objStudentDBEntities.StudentMasters
                                                                           join 
                                                                           objExam in objStudentDBEntities.Exams on objstu.ExamID  equals objExam.ExamID 
                                                                           select new StudentModel()
                                                                           {
                                                                               StudentId=objstu.StudentID,
                                                                               StudentName=objstu.Name,
                                                                               ClassName =objstu.ClassName,
                                                                               RollNumber=objstu.RollNumber,
                                                                               ExamName=objExam.ExamName,
                                                                               


                                                                           }
                                                                           ).ToList();
            return View(ObjMasterViewModel);
        }


        [HttpPost]
        public ActionResult Index(StudentViewModel objStudentViewModel)
        {

            StudentMaster _StudentMaster = new StudentMaster
            {
                Name = objStudentViewModel.StudentName,
                ClassName = objStudentViewModel.ClassName,
                ExamID = objStudentViewModel.ExamId,
                RollNumber = objStudentViewModel.RollNumber
            };
            objStudentDBEntities.StudentMasters.Add(_StudentMaster);
            objStudentDBEntities.SaveChanges();

            foreach (var item in objStudentViewModel.ListOfStudentMarks) {
                StudentsDetail _stu = new StudentsDetail()
                {
                    MarksObtained=item.MarksObtained,
                     Percentage=item.Percentage,
                     StudentId=_StudentMaster.StudentID,
                     SubjectId=item.SubjectId,
                     TotalMarks=item.TotalMarks

                };
                objStudentDBEntities.StudentsDetails.Add(_stu);
                objStudentDBEntities.SaveChanges();
            };

            return Json(data:new {message="Data Successfully Added",Status=true},behavior:JsonRequestBehavior.AllowGet);

        }


        public ActionResult StudentMarks(int studentId)
        {


            List<StudentMarksModel> _StudentMarksModel = new List<StudentMarksModel>();

            _StudentMarksModel = (from objMarks in objStudentDBEntities.StudentsDetails
                                  join objSubject in objStudentDBEntities.Subjects on objMarks.SubjectId equals objSubject.SubjectId where objMarks.StudentId==studentId
                                  select new StudentMarksModel()
                                  {
                                      SubjectName=objSubject.SubjectName,
                                       MarksObtained=objMarks.MarksObtained,
                                        TotalMarks=objMarks.TotalMarks,
                                      Percentage = objMarks.Percentage
                                  }).ToList();






            return PartialView("_StudentMarksPartial", _StudentMarksModel);
        }

    }
}